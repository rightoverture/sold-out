#!/usr/bin/env perl
use SoldOut::DB;
use SoldOUt::Config;

require './_base.cgi';
GetQuery();

$money=int($Q{money}+0);
my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);

if($Q{ok} && $money>0) {
	Lock();
	DataRead();
	CheckUserPass();
    my $shop = $db->single('shop', +{name => $USER});
	
	$money=$MAX_MONEY-$shop->money if $shop->money+$money>$MAX_MONEY;
	$money=$shop->moneystock if $money>$shop->moneystock;
	my $usetime=GetTimeDeal($money);
	OutError('時間が足りません') if GetStockTime($shop->time)<$usetime;
	
    $SHOP_DATA_NOWRITE = 1;
    eval {
        $shop->update(+{
            money => $shop->money + $money,
            moneystock => $shop->moneystock - $money,
        });
        $shop = $shop->refetch;
    };
	$DT->{money}+=$money;
	$DT->{moneystock}-=$money;
	
	UseTimeDeal($money, undef, undef, $shop);

	my $ret="\\".$money."入金しました";
	$disp.=$ret;
	WriteLog(0,$shop->id,0,$ret,1);
	
	DataWrite();
	DataCommitOrAbort();
	UnLock();
    $SHOP_DATA_NOWRITE = 0;
} else {
	DataRead();
	CheckUserPass();
    my $shop = $db->single('shop', +{name => $USER});
	$errormsg ="";
	$errormsg.='金額が不正です<br>' if $money<0;
	$errormsg.='金額が多すぎます<br>' if $money>$shop->moneystock;
	my $usetime=GetTimeDeal($money);
	$errormsg.='時間が足りません<br>' if GetStockTime($shop->time)<$usetime;
	RequireFile('inc-html-moneystock.cgi');
}

OutHTML('入金処理',$disp);
