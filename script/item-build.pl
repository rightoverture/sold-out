use Data::Dumper::Concise;
use Encode;
use SoldOut::Item::ItemFactory;

my $item = SoldOut::Item::ItemFactory->get_instance(shift || 'book-make');

warn encode_utf8($item->name);
warn Dumper $item;
