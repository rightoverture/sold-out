my $db_name = "soldout";
my $db_user = "root";
my $db_password = "";

+{
    'DBI' => [
        "dbi:mysql:dbname=$db_name",
        $db_user,
        $db_password,
    ],
    'Text::Xslate' => {
        cache_dir => '/tmp',
    },
};
