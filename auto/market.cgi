# 市場分析結果が必要な時に読み込む

# 需要/供給バランス計算
sub GetMarketStatus {
	undef %marketitEmlist;
    my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
	
	my($ITEM,$stock,$price,$mpl,$mph,$popular,$uppoint);
	
	foreach(@ITEM)
	{
		$ITEM=$_;
		$ITEM->{todaystock}=
		$ITEM->{todayprice}=
		$ITEM->{todaysale}=
		$ITEM->{yesterdaysale}=
		$ITEM->{marketprice}=
		$ITEM->{uppoint}=
		0;
	}
	
    my @shops = $db->search('shop', +{});
	foreach my $shop (@shops)
	{
        my @item_stock = $db->search('shop_item', +{shop_id => $shop->id});
        my %stocks = map { $_->item, $_->num } @item_stock;
        my @showcases = $db->search('shop_showcase', +{shop_id => $shop->id});
        foreach my $showcase (@showcases) {
            $stock = $stocks{$showcase->item};
			next if !$stock;
			$ITEM = $ITEM[$showcase->item];
			$ITEM->{todaystock}+=$stock;							# 陳列在庫トータル
			$price = $showcase->price;
			$ITEM->{todayprice}+=$price*$stock;						# 陳列金額トータル
			$mpl=\$ITEM->{marketpricelow};
			$mph=\$ITEM->{marketpricehigh};
			$$mpl=$price if $$mpl>$price || !$$mpl;	#最安値
			$$mph=$price if $$mph<$price;			#最高値
			$marketitemlist{$_}=$ITEM;
		}

        my @today_items = $db->search('shop_itemtoday', +{shop_id => $shop->id});
        foreach my $item (@today_items) {
            $ITEM = $ITEM[$item->item];
            $ITEM->{todaysale} += $item->num;						# 今期売上数
            $marketitemlist{$item->item} = $ITEM;
        }

        my @yesterday_items = $db->search('shop_itemyesterday', +{shop_id => $shop->id});
        foreach my $item (@yesterday_items) {
            $ITEM = $ITEM[$item->item];
            $ITEM->{yesterdaysale} += $item->num;					# 前期売上数
            $marketitemlist{$item->item} = $ITEM;
        }
    }

    my $people = $db->single('global', +{k => 'people'});
	my $seed=$people*$SALE_SPEED/3/50;
	foreach my $ITEM (values %marketitemlist)
	{
		$stock=$ITEM->{todaystock};
		$popular=$ITEM->{popular};
		$uppoint=!$popular ? 100 : int($stock/$seed*$popular);
		$uppoint=10   if $uppoint<10;
		$uppoint=1000 if $uppoint>1000;
		
		$ITEM->{marketprice}=int($ITEM->{todayprice}/$stock) if $stock;
		
		$ITEM->{uppoint}=$uppoint;
	}
}

# 需要/供給バランスグラフ
sub GetMarketStatusGraph
{
	my($per,$noimage)=@_;
	my $ret="";
	my $width=100;
	my $type=0;
	
	$width=int($per>1000?$width:int($per*$width/1000)),$type=1 if $per>=110;
	$width=int((100-$per+10)*$width/100),$type=2 if $per<=90;
	
	my $imgwidth=int($width/2);
	my $imgwidthl=$type==1 ? 50 : 50-$imgwidth;
	my $imgwidthr=$type==2 ? 50 : 50-$imgwidth;
	
	if(!$MOBILE && !$noimage)
	{
		if($type)
		{
			$ret.=qq|<img src="$IMAGE_URL/t$IMAGE_EXT" width="$imgwidthl" height="12">| if $imgwidthl;
			$ret.=" 飽和 " if $imgwidthl==50;
			$ret.=qq|<img src="$IMAGE_URL/|.('b','r')[$type-1].qq|$IMAGE_EXT" width="$imgwidth" height="12">|;
			$ret.=" 不足 " if $imgwidthr==50;
			$ret.=qq|<img src="$IMAGE_URL/t$IMAGE_EXT" width="$imgwidthr" height="12">| if $imgwidthr;
		}
		else
		{
			$ret.=qq|<img src="$IMAGE_URL/t$IMAGE_EXT" width="50" height="12"> 均衡 |;
			$ret.=qq|<img src="$IMAGE_URL/t$IMAGE_EXT" width="50" height="12">|;
		}
	}
	else
	{
		$width=!$type ? "" : " $width%";
		$ret.=('均衡 ','飽和 ','不足 ')[$type].$width;
	}
	return $ret;
}


1;
