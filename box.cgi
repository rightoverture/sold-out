#!/usr/bin/env perl
use SoldOut::DB;
use SoldOUt::Config;

require './_base.cgi';
GetQuery();

DataRead();
CheckUserPass();

ReadBox();

GetInBox();
GetOutBox();
GetRetBox();
RequireFile('inc-html-box.cgi');

OutHTML('郵便箱',$disp);
exit;
