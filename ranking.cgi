#!/usr/bin/env perl
use SoldOut::DB;
use SoldOUt::Config;

require './_base.cgi';

GetQuery();

DataRead();
CheckUserPass(1);

RequireFile('inc-html-ranking.cgi');

OutHTML('ランキング',$disp);
exit;
