package SoldOut::Game;
use strict;
use warnings;
use SoldOut::Config;
use SoldOut::YAML::Loader;
use Data::Dumper;

my $yaml = SoldOut::YAML::Loader->load(SoldOut::Config::GAME_CONFIG);

while (my ($key, $value) = each $yaml) {
    {
        no strict 'refs';
        no warnings 'redefine';
        *{__PACKAGE__."::".uc($key)} = sub {
            return $value;
        };
    }
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Game - Gameに関連した設定を保持する

=head1 SYNOPSIS

    use SoldOut::Game;

=head1 AUTHOR

Fumihiro Itoh

=cut
