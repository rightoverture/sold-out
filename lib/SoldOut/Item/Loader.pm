package SoldOut::Item::Loader;
use strict;
use warnings;
use SoldOut::Config;

use parent qw/SoldOut::YAML::Loader/;

sub load {
    my $class = shift;
    my $code = shift;

    return $class->SUPER::load(SoldOut::Config::ITEM_CONFIG_PATH.$code);
}

1;
__END__

=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Loader - YAMLからアイテムの情報を読み込むクラス

=head1 SYNOPSIS

    my $item_hash = SoldOut::Item::Loader->load("book");

=head1 METHOD

=over 4

=item load

return: HASHREF

指定されたコードのアイテムを読み込む

=back

=head1 AUTHOR

Fumihiro Ito

=cut
