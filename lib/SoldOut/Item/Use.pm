package SoldOut::Item::Use;
use Mouse;

has 'time'     => (is => 'ro', isa => 'Str');
has 'exp'      => (is => 'ro', isa => 'Int');
has 'exptime'  => (is => 'ro', isa => 'Int');
has 'job'      => (is => 'ro', isa => 'Str');
has 'scale'    => (is => 'ro', isa => 'Str');
has 'action'   => (is => 'ro', isa => 'Str');
has 'name'     => (is => 'ro', isa => 'Str');
has 'info'     => (is => 'ro', isa => 'Str');
has 'okmsg'    => (is => 'ro', isa => 'Str');
has 'ngmsg'    => (is => 'ro', isa => 'Str');
has 'requires' => (is => 'ro');
has 'result'   => (is => 'ro');

sub can_use {
    my $self = shift;
    my $shop = shift;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Use - 

=head1 AUTHOR

Fumihiro Ito

=cut
