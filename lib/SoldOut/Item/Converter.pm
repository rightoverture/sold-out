package SoldOut::Item::Converter;
use strict;
use warnings;
use SoldOut::YAML::Loader;
use Data::Dumper;

my $TABLE = SoldOut::YAML::Loader->load("item_table");

sub no2code($) {
    my $number = shift;

    return $TABLE->{$number};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Converter -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

=cut
