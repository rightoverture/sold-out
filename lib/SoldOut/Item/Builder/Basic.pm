package SoldOut::Item::Builder::Basic;
use strict;
use warnings;
use Mouse;

use SoldOut::Item::Object;
use SoldOut::Item::Use;

with 'SoldOut::Item::Builder::Role';

sub load {
    my $self = shift;

    my @use_action = map {SoldOut::Item::Use->new($_)} @{$self->{_params}->{use}};
    $self->{_params}->{use} = \@use_action;

    $self->{_object} = SoldOut::Item::Object->new($self->{_params});
}

sub make_function {
    1;
}

sub make_action {
    my $self = shift;
}

sub get_result {
    my $self = shift;

    return $self->{_object};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Builder::Basic - 基本的なItem Builder

=head1 SYNOPSIS

=head1 AUTHOR

Fumihiro Ito

=cut
