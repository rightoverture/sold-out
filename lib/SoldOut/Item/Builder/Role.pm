package SoldOut::Item::Builder::Role;
use strict;
use warnings;
use Mouse::Role;

requires qw/
    load
    make_function
    make_action
    get_result
/;

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Builder::Role - interface of SoldOut::Item::Builder

=head1 DESCRIPTION

This class is an interface of SoldOut::Item::Builder.

=head1 SYNOPSIS

    package SoldOut::Item::Builder::Basic;
    use Mouse;

    with 'SoldOut::Item::Builder::Role';

    sub load { ...  }
    sub make_function { ...  }
    sub make_action { ...  }
    sub get_result { ... }
    1;

=head1 METHOD

=over 4

=item load

=item make_function

=item make_action

=item get_result

=back

=head1 AUTHOR

Fumihiro Ito

=cut
