package SoldOut::Item::ItemFactory;
use strict;
use warnings;

use SoldOut::Item::Director;

my %pool;

sub get_instance($) {
    my $self = shift;
    my $code = shift;

    if (not exists $pool{$code}) {
        my $director = SoldOut::Item::Director->new(code => $code);
        $pool{$code} = $director->construct;
    }

    return $pool{$code};
}

1;
__END__

=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::ItemFactory - アイテムオブジェクトを生成する

=head1 SYNOPSIS

    use SoldOut::Item::ItemFactory;
    my $book = SoldOut::Item::ItemFactory->get_instance("book");

=head1 DESCRIPTION

FlyweightパターンのFactoryオブジェクトに該当するクラス
アイテムオブジェクトを生成する場合は本オブジェクトより生成する

=head1 METHOD

=over 4

=item get_instance

既存のインスタンスがなければ生成し，ある場合はそれを返す

=back

=cut
