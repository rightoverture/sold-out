package SoldOut::Item::Director;
use strict;
use warnings;
use UNIVERSAL::require;

use SoldOut::Item::Loader;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = bless {}, $class;

    my $config = SoldOut::Item::Loader->load($args{code});

    my $builder_class = "SoldOut::Item::Builder::".$config->{builder};
    $builder_class->require;
    $self->{_builder} = $builder_class->new;
    $self->{_builder}->{_params} = $config->{params};

    return $self;
}

sub construct {
    my $self = shift;
    my $code = shift;

    $self->{_builder}->load($code);
    $self->{_builder}->make_function;
    $self->{_builder}->make_action;

    return $self->{_builder}->get_result;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Director - SoldOut::Item::BuilderのDirector

=head1 SYNOPSIS

    my $director = SoldOut::Item::Director->new(
        builder => SoldOut::Item::Builder::Basic->new
    );
    my $item = $director->construct($code);
    $item; # SoldOut::Item::Object

=head1 AUTHOR

Fumihiro Ito

=cut
