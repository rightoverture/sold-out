package SoldOut::Item::Image;
use strict;
use warnings;
use SoldOut::Config;

sub get_type_image {
    my $type = shift;

    return SoldOut::Config::IMAGE_PATH."/item-typesign".$type.".png";
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Image -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
