package SoldOut::Item::Object;
use Mouse;

has 'code'       => (is => 'ro', isa => 'Str');
has 'sort'       => (is => 'ro', isa => 'Int');
has 'type'       => (is => 'ro', isa => 'Int');
has 'price'      => (is => 'ro', isa => 'Int');
has 'cost'       => (is => 'ro', isa => 'Int');
has 'pricebase'  => (is => 'ro', isa => 'Int');
has 'pricehalf'  => (is => 'ro', isa => 'Int');
has 'limit'      => (is => 'ro', isa => 'Str');
has 'wslimit'    => (is => 'ro', isa => 'Int');
has 'popular'    => (is => 'ro', isa => 'Int');
has 'plus'       => (is => 'ro', isa => 'Str');
has 'code'       => (is => 'ro', isa => 'Str');
has 'scale'      => (is => 'ro', isa => 'Str');
has 'name'       => (is => 'ro', isa => 'Str');
has 'info'       => (is => 'ro', isa => 'Str');
has 'point'      => (is => 'ro', isa => 'Int');
has 'param'      => (is => 'ro', isa => 'Int');
has 'existimage' => (is => 'ro', isa => 'Str');
has 'func'       => (is => 'ro', isa => 'Int');
has 'functurn'   => (is => 'ro', isa => 'Int');
has 'funcsale'   => (is => 'ro', isa => 'Int');
has 'funcbuy'    => (is => 'ro', isa => 'Int');
has 'flag'       => (is => 'ro', isa => 'Int');
has 'image'      => (is => 'ro', isa => 'Str');
has 'base'       => (is => 'ro', isa => 'Str');
has 'use'        => (is => 'ro');

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Item::Object - アイテムの抽象クラス

=head1 DESCRIPTION

アイテムの情報を保持するトップクラス．アイテムオブジェクトは同一のアイテムに
対して複数生成しない．
またオブジェクトの生成はFactoryを通して行う．

=head1 AUTHOR

Fumihiro Ito

=cut
