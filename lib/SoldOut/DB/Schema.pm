package SoldOut::DB::Schema;
use strict;
use warnings;
use Teng::Schema::Declare;

table {
    name 'item';
    pk 'no';
    columns qw/
        no
        sort
        type
        price
        cost
        pricebase
        pricehalf
        limit
        wslimit
        popular
        plus
        code
        scale
        name
        info
        point
        param
        existimage
        func
        functurn
        funcsale
        funcbuy
        flag
    /;
};

table {
    name 'setting';
    pk 'k';
    columns qw/k v/;
};

table {
    name 'event';
    pk 'id';
    columns qw/
        id
        code
        group
        startproba
        startfunc
        startfuncparam
        endfunc
        endfuncparam
        basetime
        plustime
        startmsg
        endmsg
        func
        funcparam
    /;
};

table {
    name 'global';
    pk 'k';
    columns qw/k v/;
};

table {
    name 'market';
    pk 'item_no';
    columns qw/
        item_no
        num
    /;
};

table {
    name 'shop';
    pk 'id';
    columns qw/
        id
        lastlogin
        name
        shopname
        pass
        money
        time
        rank
        rank_order
        showcasecount
        comment
        saleyesterday
        saletoday
        costyesterday
        rankingcount
        remoteaddr
        pointyesterday
        rankingyesterday
        paytoday
        payyesterday
        profitstock
        boxcount
        costtoday
        options
        taxyesterday
        moneystock
        taxtoday
        guild
        blocklogin
        nocheckip
        foundation
        job
        icon
        point
    /;
};

table {
    name 'shop_showcase';
    pk 'shop_id', 'id';
    columns qw/
        id
        shop_id
        item
        price
    /;
};

table {
    name 'shop_item';
    pk 'shop_id', 'item';
    columns qw/
        shop_id
        item
        num
        exp
        sales_today
        sales_yesterday
    /;
};

table {
    name 'shop_itemyesterday';
    pk 'shop_id', 'item';
    columns qw/
        shop_id
        item
        num
    /;
};

table {
    name 'shop_itemtoday';
    pk 'shop_id', 'item';
    columns qw/
        shop_id
        item
        num
    /;
};

table {
    name 'shop_exp';
    pk 'shop_id', 'item';
    columns qw/
        shop_id
        item
        exp
    /;
};

table {
    name 'log';
    pk 'id';
    columns qw/
        id
        type
        mode
        send_from
        send_to
        message
        insert_date
    /;
};

1;
