package SoldOut::Util::Datetime;
use strict;
use warnings;

sub epoch2hms {
    my $epoch = shift;

    my $sec = $epoch % 60;
    return sprintf("%02d秒", $sec) if $epoch < 60;

    my $min = ($epoch - $sec) % 3600;
    return sprintf("%d分", $min/60) if $epoch < 3600;

    my $hour = ($epoch - $sec - $min) / 3600;
    if ($hour < 24 * 3) {
        my $result = sprintf("%d時間", $hour);
        $result .= sprintf("%02d分", $min / 60) if $min;
        return $result;
    }

    return sprintf("%d日", $hour / 24);
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Util::Datetime -

=head1 METHOD

=head2 epoch2hms

可読表現を返す

=head1 AUTHOR

Fumihiro Itoh

=cut
