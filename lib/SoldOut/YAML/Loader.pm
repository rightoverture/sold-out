package SoldOut::YAML::Loader;
use YAML;

sub load {
    my $class = shift;
    my $name = shift;

    return YAML::LoadFile($class->_load_path($name))
}

sub _load_path {
    my $class = shift;
    my $name = shift;

    return SoldOut::Config::CONFIG_PATH.$name.SoldOut::Config::YAML_EXT;
}

1;
__END__

=pod

=encoding utf-8

=head1 NAME

SoldOut::YAML::Loader - YAMLを読み込む基底クラス

=head1 AUTHOR

Fumihiro Ito

=cut
