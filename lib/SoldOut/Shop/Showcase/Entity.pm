package SoldOut::Shop::Showcase::Entity;
use strict;
use warnings;
use Mouse;

has id => (
    is      => 'ro',
    isa     => 'Int'
);

has shop_id => (
    is      => 'ro',
    isa     => 'Int'
);
has code => (
    is      => 'rw',
    isa     => 'Str'
);
has item => (
    is      => 'rw',
    isa     => 'SoldOut::Item::Object'
);
has item_id => (
    is      => 'ro',
    isa     => 'Int',
);
has price => (
    is      => 'rw',
    isa     => 'Int'
);

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Showcase::Entity - 陳列棚情報を保持するクラス

=head1 AUTHOR

Fumihiro Ito

=cut
