package SoldOut::Shop::Showcase::Fetcher;
use strict;
use warnings;
use SoldOut::Config;
use SoldOUt::DB;
use SoldOUt::Shop::Showcase::Object;
use SoldOut::Item::ItemFactory;
use SoldOut::Item::Converter;
use Data::Dumper;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = bless {}, $class;

    if (exists $args{dbh}) {
        $self->{_db} = SoldOut::DB->new(dbh => $args{dbh});
    } else {
        $self->{_db} = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }

    return $self;
}

sub fetch($) {
    my $self = shift;
    my $shop_id = shift;

    my @showcase = $self->{_db}->search(SoldOut::Config::SHOWCASE_TABLE, +{shop_id => $shop_id});
    my @entity = map {
        SoldOut::Shop::Showcase::Object->new(%$_);
    } map {
        my $code = SoldOut::Item::Converter::no2code($_->{row_data}->{item});
        +{
            id => $_->{row_data}->{id},
            shop_id => $_->{row_data}->{shop_id},
            code => $code,
            item => SoldOut::Item::ItemFactory->get_instance($code),
            item_id => $_->{row_data}->{item},
            price => $_->{row_data}->{price},
        };
    } grep {
        $_->{row_data}->{item} ne "0";
    } @showcase;

    return wantarray ? @entity : \@entity;
}

sub db {
    my $self = shift;

    return $self->{_db};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Showcase::Fetcher -

=head1 SYNOPSIS

    use SoldOut::Shop::Showcase::Fetcher;

    my @showcase = SoldOut::Shop::Showcase::Fetcher->new(dbh => $dbh)->fetch($shop_id);

or create db handler by constructor of showcase fetcher

    my @showcase = SoldOut::Shop::Showcase::Fetcher->new->fetch($shop_id);

=head1 DESCRIPTION

=head1 METHOD

=head2 new

クラスのインスタンスを返す．この時点でDBとの接続を確率する．

=head2 fetch($shop_id)

そのショップのショーケースを取得する．
返り値はL<SoldOUt::Shop::Showcase::Entity>のB<arrayref>である．

=head1 AUTHOR

Fumihiro Ito

=cut
