package SoldOut::Shop::Showcase::Object;
use strict;
use warnings;
use Mouse;
use SoldOut::Game;

extends 'SoldOut::Shop::Showcase::Entity';

# This method will be move to.
sub get_cost {
    my $self = shift;
    my $showcase_count = shift;

    my $cost = SoldOut::Game::SHOWCASE_COST;
    return $cost->{$showcase_count-1};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Showcase -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head2 count

棚の数を返す

=head1 AUTHOR

Fumihiro Itoh

=cut
