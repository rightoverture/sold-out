package SoldOut::Shop::Stock::Entity;
use strict;
use warnings;
use Mouse;

has 'item'      => (isa => 'Int');
has 'num'       => (isa => 'Int');
has 'exp'       => (isa => 'Int');
has 'yesterday' => (isa => 'Int');
has 'today'     => (isa => 'Int');

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Stock::Entity - 在庫情報を保持するクラス

=head1 AUTHOR

Fumihiro Ito

=cut
