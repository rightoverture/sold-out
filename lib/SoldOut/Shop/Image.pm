package SoldOut::Shop::Image;
use strict;
use warnings;
use Data::Dumper;
use SoldOut::Config;
use SoldOut::Game;

sub get_shop_icon {
    my $shop = shift;

    return if not defined $shop->icon;

    return SoldOut::Config::IMAGE_PATH."/shop-".$shop->icon.".png";
}

sub get_rank_icon {
    my $shop = shift;

    my $top_ranking_count = SoldOut::Game::TOP_COUNT_IMAGE_LIST;
    my @result;
    for (my $i = 0; $i < scalar @$top_ranking_count; $i++) {
        last if $shop->rankingcount < $top_ranking_count->[$i];
        push @result, SoldOut::Config::IMAGE_PATH."/rank".($i+1).".png";
    }

    return wantarray ? @result : \@result;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Image -

=head1 METHOD

=head2 get_shop_icon

ショップのiconファイル名を返す

=head2 get_rank_icon

ランキングに応じた画像のリストを返す

=head1 AUTHOR

Fumihiro Itoh

=cut
