package SoldOut::Shop::Fetcher;
use strict;
use warnings;
use SoldOut::Config;
use SoldOut::DB;
use SoldOut::Shop::Entity;
use SoldOut::Shop::Object;
use Data::Dumper;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = bless {}, $class;

    if (exists $args{dbh}) {
        $self->{_db} = SoldOut::DB->new(dbh => $args{dbh});
    } else {
        $self->{_db} = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }

    return $self;
}

sub fetch($) {
    my $self = shift;
    my $shop_id = shift;

    my $shop = $self->db->single(SoldOut::Config::SHOP_TABLE(), +{id => $shop_id});

    return $self->create_entity(%{$shop->{row_data}});
}

sub fetch_by_username($) {
    my $self = shift;
    my $username = shift;

    my $shop = $self->db->single(SoldOut::Config::SHOP_TABLE(), +{name => $username});

    return $self->create_entity(%{$shop->{row_data}});
}

sub create_entity {
    my $self = shift;

    return SoldOut::Shop::Object->new(@_);
}

sub db {
    my $self = shift;

    return $self->{_db};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Fetcher -

=head1 SYNOPSIS

    use SoldOut::Shop::Fetcher;

    my $shop = SoldOut::Shop::Fetcher->new(dbh => $dbh)->fetch_by_username($username);

=head1 METHOD

=head2 new

クラスのインスタンスを返す．この時点でDBとの接続を行う．

=head2 fetch_by_username($username)

DBから店舗の情報を取得し，L<SoldOut::Shop::Entity>を返す．

=head1 AUTHOR

Fumihiro Ito

=cut
