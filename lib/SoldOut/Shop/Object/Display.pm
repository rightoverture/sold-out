package SoldOut::Shop::Object::Display;
use strict;
use warnings;
use Mouse;

extends 'SoldOut::Shop::Object';

has rank_image => (
    is      => 'ro',
    isa     => 'ArrayRef',
);

has money_str => (
    is      => 'ro',
    isa     => 'Str',
);

has popular_bar => (
    is      => 'ro',
);

has foundation_days => (
    is      => 'ro',
    isa     => 'Str',
);

has type_sign => (
    is      => 'ro',
);

no Mouse;

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Object::Display -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
