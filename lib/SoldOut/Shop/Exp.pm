package SoldOut::Shop::Exp;
use strict;
use warnings;
use Mouse;
use SoldOut::Shop::Exp::Fetcher;

has shop_id => (
    is      => 'ro',
    isa     => 'Int',
);

has db => (
    is      => 'rw',
    lazy    => 1,
    builder => '_build_db'
);

sub _build_db {
    my $self = shift;

    return SoldOut::Shop::Exp::Fetcher->new;
}

sub sum {
    my $self = shift;

    return $self->db->fetch_sum_exp($self->shop_id);
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Exp -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
