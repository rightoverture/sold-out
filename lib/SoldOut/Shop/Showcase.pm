package SoldOut::Shop::Showcase;
use strict;
use warnings;
use Mouse;

has entities => (
    is      => 'rw',
    isa     => 'ArrayRef',
);

sub get {
    my $self = shift;
    my $index = shift;

    return $self->entities->[$index];
}

sub as_array {
    my $self = shift;

    return @{$self->entities};
}

sub as_arrayref {
    my $self = shift;

    return $self->entities;
}

sub item_count {
    my $self = shift;

    return scalar @{$self->entities};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Showcase -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
