package SoldOut::Shop::Exp::Fetcher;
use strict;
use warnings;
use SoldOut::Config;
use SoldOUt::DB;
use SoldOUt::Shop::Exp::Entity;
use SoldOut::Item::ItemFactory;
use SoldOut::Item::Converter;
use Data::Dumper;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = bless {}, $class;

    if (exists $args{dbh}) {
        $self->{_db} = SoldOut::DB->new(dbh => $args{dbh});
    } else {
        $self->{_db} = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }

    return $self;
}

sub fetch_sum_exp($) {
    my $self = shift;
    my $shop_id = shift;

    my $dbh = $self->db->dbh;
    my $sth = $dbh->prepare(
        qq|SELECT sum(exp) AS exp FROM |.SoldOut::Config::SHOP_ITEM_TABLE.qq| WHERE shop_id = ?|);
    $sth->execute($shop_id);

    my $result = $sth->fetchrow_arrayref;
    return defined $result->[0] ? $result->[0] : "0";
}

# This method will be remove.
sub fetch($) {
    my $self = shift;
    my $shop_id = shift;
    warn "deprecated...";

    my @exp = $self->db->search(SoldOut::Config::EXP_TABLE, +{shop_id => $shop_id});
    my @entity = map {
        SoldOut::Shop::Exp::Entity->new(%$_);
    } map {
        my $code = SoldOut::Item::Converter::no2code($_->{row_data}->{item});
        +{
            shop_id => $_->{row_data}->{shop_id},
            code => $code,
            item => SoldOut::Item::ItemFactory->get_instance($code),
            exp => $_->{row_data}->{exp},
        };
    } @exp;

    return wantarray ? @entity : \@entity;
}

sub db {
    my $self = shift;

    return $self->{_db};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Exp::Fetcher -

=head1 SYNOPSIS

    use SoldOut::Shop::Exp::Fetcher;

    my @showcase = SoldOut::Shop::Exp::Fetcher->new(dbh => $dbh)->fetch($shop_id);

or create db handler by constructor of showcase fetcher

    my @showcase = SoldOut::Shop::Exp::Fetcher->new->fetch($shop_id);

=head1 DESCRIPTION

=head1 METHOD

=head2 new

クラスのインスタンスを返す．この時点でDBとの接続を確率する．

=head2 fetch($shop_id)

そのショップの熟練度を返す
返り値はL<SoldOUt::Shop::Exp::Entity>のB<arrayref>である．

=head1 AUTHOR

Fumihiro Ito

=cut
