package SoldOut::Shop::Exp::Entity;
use strict;
use warnings;
use Mouse;

has 'shop_id' => (is => 'ro', isa => 'Int');
has 'code'    => (is => 'rw', isa => 'Str');
has 'item'    => (is => 'rw', isa => 'SoldOut::Item::Object');
has 'exp'   => (is => 'rw', isa => 'Int');

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Exp::Entity - 陳列棚情報を保持するクラス

=head1 AUTHOR

Fumihiro Ito

=cut
