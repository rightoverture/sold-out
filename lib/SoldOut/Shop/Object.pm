package SoldOut::Shop::Object;
use strict;
use warnings;
use Mouse;
use SoldOut::Shop::Fetcher;
use SoldOut::Shop::Showcase;
use SoldOut::Shop::Showcase::Fetcher;
use SoldOut::Shop::Util;
use SoldOut::Shop::Exp;
use SoldOut::Shop::Item;
use Data::Dumper;

extends 'SoldOut::Shop::Entity';

has items => (
    is         => 'ro',
    lazy_build => 1,
);

has exp => (
    is         => 'ro',
    lazy_build => 1,
);

sub _build_items {
    my $self = shift;

    return SoldOut::Shop::Item->new(shop_id => $self->id);
}

sub _build_exp {
    my $self = shift;

    return SoldOut::Shop::Exp->new(shop_id => $self->id);
}

sub showcase {
    my $self = shift;

    if (not exists $self->{showcase_entity}) {
        my @showcase = SoldOut::Shop::Showcase::Fetcher->new()->fetch($self->id);
        $self->{showcase_entity} = \@showcase;
    }
    return SoldOut::Shop::Showcase->new(entities => $self->{showcase_entity});
}

sub point {
    my $self = shift;

    return int($self->rank*(($self->profitstock*2+$self->saletoday-$self->taxtoday+$self->saleyesterday-$self->taxyesterday)/400000+1));
}

sub action_time {
    my $self = shift;

    my $time = time - $self->time;
    return ($time > SoldOut::Game::MAX_STOCK_TIME)
            ? SoldOut::Game::MAX_STOCK_TIME
            : $time;
}

sub action_time_str {
    my $self = shift;

    my $action_time = $self->action_time;

    return SoldOut::Util::Datetime::epoch2hms($action_time);
}

sub foundation_str {
    my $self = shift;

    return SoldOut::Util::Datetime::epoch2hms(time - $self->foundation);
}

sub tax_rate {
    my $self = shift;

    my $taxrate = int($self->profitstock / 20000);
    $taxrate = 0 if $taxrate < 0;
    $taxrate = 50 if $taxrate > 50;

    return $taxrate;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Object -

=head1 SYNOPSIS

=head1 DESCRIPTION

店舗クラス

=head1 METHOD

=head2 create_by_name

Deprecated

ユーザー名から店舗クラスを構築する．
返り値はL<SoldOut::Shop::Entity>である．

=head2 showcase

Lazy-load

L<SoldOut::Shop::Showcase>オブジェクトを返す．

=head2 point

点数を計算して返す

=head2 action_time

持ち時間を計算して返す(秒)

=head2 action_time_str

持ち時間の可読表現を返す

=head2 foundation_str

創業の可読表現を返す

=head2 tax_rate

店舗売却税率を返す

=head1 AUTHOR

Fumihiro Ito

=cut
