package SoldOut::Shop::Fetcher::Single;
use strict;
use warnings;
use parent qw/SoldOut::Shop::Fetcher/;

sub fetch {
    my $self = shift;
    my $shop_id = shift;

    my $result = $self->db->single(SoldOut::Config::SHOP_TABLE, +{
        id => $shop_id,
    });

    return $result->{row_data};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Fetcher::Single -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
