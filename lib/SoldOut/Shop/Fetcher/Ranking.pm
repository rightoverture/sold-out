package SoldOut::Shop::Fetcher::Ranking;
use strict;
use warnings;
use Data::Dumper;

use parent qw(SoldOut::Shop::Fetcher);

sub fetch {
    my $self = shift;

    my @result = $self->db->search(SoldOut::Config::SHOP_TABLE, +{}, +{
        order_by => [{'rankingyesterday' => 'ASC'}]
    });
    my @entities = map { $self->create_entity(%{$_->{row_data}}) } @result;
    my @sorted_by_point = sort { $b->point <=> $a->point } @entities;

    foreach my $index (0..$#sorted_by_point) {
        my $shop = $sorted_by_point[$index];
        my $rankupdown = $shop->rankingyesterday - $index - 1;
        $shop->rankupdown($rankupdown == 0 ? "stay" : $rankupdown < 0 ? "down" : "up");
    }

    return wantarray ? @sorted_by_point : \@sorted_by_point;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Fetcher::Ranking - fetcher by ranking

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head2 fetch

ランキングを元にL<SoldOut::Shop::Entity>のリストを返す

=head1 AUTHOR

Fumihiro Itoh

=cut
