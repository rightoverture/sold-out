package SoldOut::Shop::Util;
use strict;
use warnings;
use utf8;
use SoldOut::Game;
use Date::Calc;

sub money2str($) {
    my $money = int(($_[0]+9999)/10000);

    my $band = SoldOut::Game::MONEY_MESSAGE_BAND;
    foreach my $rank (@$band) {
        return $rank."万円以下" if $money <= $rank;
    }
}

sub get_popular_bar {
    my $per = int($_[0]/100);

    return +{
        b => $per,
        r => 100 - $per,
        per => $per,
    };
}

sub calc_foundation_days {
    my $shop_foundation = shift;

    my ($year, $month, $day) = Date::Calc::Localtime($shop_foundation);
    return Date::Calc::Delta_Days($year, $month, $day, Date::Calc::Today);
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Util - Utilities of shop

=head1 SYNOPSIS

    use SoldOut::Shop::Uitl;

=head1 METHOD

=head2 money2str

表示用にお店が持っている資金を丸める

=head2 get_popular_bar

人気グラフの割合を返す

=head2 calc_foundation_days

創業からの日数を返す

=head1 AUTHOR

Fumihiro Itoh

=cut
