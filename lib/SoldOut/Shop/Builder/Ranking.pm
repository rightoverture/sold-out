package SoldOut::Shop::Builder::Ranking;
use strict;
use warnings;
use Mouse;
use List::MoreUtils qw//;
use SoldOut::Shop::Fetcher::Ranking;
use SoldOut::Shop::Image;
use SoldOut::Shop::Object::Display;
use SoldOut::Shop::Util;
use SoldOut::Item::Image;
use Data::Dumper;

with 'SoldOut::Shop::Builder::Base';

sub fetcher {
    my $self = shift;

    my @ranking = SoldOut::Shop::Fetcher::Ranking->new->fetch;
    return \@ranking;
}

sub transform {
    my $self = shift;
    my $raw_data = shift;

    my @result;
    foreach my $each_shop (@$raw_data) {
        my @rank_image = SoldOut::Shop::Image::get_rank_icon($each_shop);
        my $type_sign = undef;
        my @showcase = $each_shop->showcase->as_array;
        if (scalar @showcase > 0 and
                List::MoreUtils::all {$showcase[0]->item->type eq $_->item->type} @showcase) {
            $type_sign = SoldOut::Item::Image::get_type_image($showcase[0]->item->type);
        }

        push @result, +{
            %$each_shop,
            rank_image => \@rank_image,
            money_str => SoldOut::Shop::Util::money2str($each_shop->money),
            popular_bar => SoldOut::Shop::Util::get_popular_bar($each_shop->rank),
            foundation_days => SoldOut::Shop::Util::calc_foundation_days($each_shop->foundation),
            type_sign => $type_sign,
        };
    }

    return \@result;
}

sub blessing {
    my $self = shift;
    my $data = shift;

    my @result = map { SoldOut::Shop::Object::Display->new(%$_) } @$data;

    return \@result;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Builder::Ranking -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
