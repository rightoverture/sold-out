package SoldOut::Shop::Builder::Owner;
use strict;
use warnings;
use Mouse;
use SoldOut::Shop::Object;
use SOldOut::Shop::Fetcher::Single;

with 'SoldOut::Shop::Builder::Base';

sub fetcher {
    my $self = shift;
    my $shop_id = shift;

    return SoldOut::Shop::Fetcher::Single->new->fetch($shop_id);
}

sub transform {
    my $self = shift;

    return shift
}

sub blessing {
    my $self = shift;
    my $data = shift;

    return SoldOut::Shop::Object->new(%$data);
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Builder::Owner -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
