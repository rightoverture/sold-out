package SoldOut::Shop::Builder::Director;
use strict;
use warnings;
use Mouse;

has builder => (
    is      => 'ro',
    does    => 'SoldOut::Shop::Builder::Base',
);

sub construct {
    my $self = shift;

    $self->{_raw} = $self->builder->fetcher(@_);
    $self->{_data} = $self->builder->transform($self->{_raw});
    return $self->builder->create_entity($self->{_data});
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Builder::Director - Director of Shop::Builder's

=head1 SYNOPSIS

    use SoldOut::Shop::Builder::Director;
    use SoldOut::Shop::Builder::Sample;

    my $director = SoldOut::Shop::Builder::Director->new(
        builder => SoldOut::Shop::Builder::Sample->new,
    );
    my $sample_shop = $director->construct(shop_id => 1234);

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
