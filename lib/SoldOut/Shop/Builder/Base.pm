package SoldOut::Shop::Builder::Base;
use strict;
use warnings;
use Mouse::Role;

requires 'fetcher';
requires 'transform';
requires 'blessing';

sub create_entity {
    my $self = shift;
    my $data = shift;

    return $self->blessing($data);
}

no Mouse::Role;
1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Builder::Base - abstract class of Shop::Builder

=head1 SYNOPSIS

    package SoldOut::Shop::Builder::Sample;
    use Mouse;

    with 'SoldOut::Shop::Builder::Base';

    sub fetcher {
        my $self = shift;

        # some code...
        return $raw_data;
    }

    sub transform {
        my $self = shift;
        my $raw_data = shift;
        
        # some code...
        return $data;
    }

    sub blessing {
        my $self = shift;
        my $data = shift;

        # some code...
        return $entity;
    }

    no Mouse;
    1;

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
