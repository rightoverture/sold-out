package SoldOut::Shop::Create;
use strict;
use warnings;
use SoldOut::YAML::Loader;
use SoldOut::Config;
use SoldOut::DB;
use SoldOut::Shop::Object;
use Data::Dumper;

sub setup {
    my $self = shift;
    my %params = @_;

    my $initial_data = SoldOut::YAML::Loader->load("init_shop");
    my $result = $self->db->insert(SoldOut::Config::SHOP_TABLE, +{
        %$initial_data,
        name => $params{username},
        name => $params{shop_name},
        pass => $params{password},
        foundation => $params{foundation},
        lastlogin => $params{foundation},
        time => $params{foundation} - 12*60*60,
        remoteaddr => $params{remote_addr},
    });
    return SoldOut::Shop::Object->new(%{$result->{row_data}});
}

sub new {
    my $class = shift;
    my %args = @_;
    my $self = bless {}, $class;

    if (exists $args{dbh}) {
        $self->{_db} = SoldOut::DB->new(dbh => $args{dbh});
    } else {
        $self->{_db} = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }

    return $self;
}

sub db {
    my $self = shift;

    return $self->{_db};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Create -

=head1 SYNOPSIS

    use SoldOut::Shop::Create;

    my $result = SoldOut::Shop::Create->new->setup(
        shop_name => $shop_name,
        username => $unsername,
        password => $password,
        foundation => time(),
    );

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
