package SoldOut::Shop::Item;
use strict;
use warnings;
use Mouse;
use SoldOut::Shop::Item::Fetcher;

has shop_id => (
    is      => 'ro',
    isa     => 'Int',
);

has fetcher => (
    is         => 'ro',
    lazy_build => 1,
);

sub fetch_all_as_arrayref {
    my $self = shift;

    my @entities = $self->fetcher->fetch_all($self->shop_id);

    return \@entities;
}

sub fetch_one {
    my $self = shift;
    my $item_id = shift;

    return $self->fetcher->fetch($self->shop_id, $item_id);
}

sub _build_fetcher {
    my $self = shift;

    return SoldOut::Shop::Item::Fetcher->new;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Item -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Ito

=cut
