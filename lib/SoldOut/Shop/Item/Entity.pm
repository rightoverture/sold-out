package SoldOut::Shop::Item::Entity;
use strict;
use warnings;
use Mouse;

has item => (
    is      => 'ro',
    isa     => 'Int',
);

has num => (
    is      => 'ro',
    isa     => 'Int',
);

has exp => (
    is      => 'ro',
    isa     => 'Int',
);

has sales_today => (
    is      => 'ro',
    isa     => 'Int',
);

has sales_yesterday => (
    is      => 'ro',
    isa     => 'Int',
);

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Item::Entity -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Ito

=cut
