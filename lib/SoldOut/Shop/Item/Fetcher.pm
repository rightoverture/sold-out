package SoldOut::Shop::Item::Fetcher;
use strict;
use warnings;
use Mouse;
use SoldOut::Config;
use SoldOut::Shop::Item::Entity;
use Data::Dumper;

has _dbh => (
    is      => 'ro',
);

has db => (
    is         => 'ro',
    lazy_build => 1
);

sub fetch($$) {
    my $self = shift;
    my $shop_id = shift;
    my $item_id = shift;

    my $row = $self->db->single(SoldOut::Config::SHOP_ITEM_TABLE, +{
        shop_id => $shop_id,
        item => $item_id,
    });

    return SoldOut::Shop::Item::Entity->new(%{$row->{row_data}});
}

sub fetch_all {
    my $self = shift;
    my $shop_id = shift;

    my @result = $self->db->search(SoldOut::Config::SHOP_ITEM_TABLE, +{
        shop_id => $shop_id,
    });
    my @entities = map { SoldOut::Shop::Item::Entity->new(%{$_->{row_data}}) } @result;

    return @entities;
}

sub _build_db {
    my $self = shift;

    if ($self->_dbh) {
        return SoldOut::DB->new(dbh => $self->_dbh);
    } else {
        return SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Item::Fetcher -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
