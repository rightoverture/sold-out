package SoldOut::Shop::Entity;
use strict;
use warnings;
use Mouse;
use SoldOut::Util::Datetime;

has 'id'               => (is => 'ro', isa => 'Int');
has 'lastlogin'        => (is => 'rw', isa => 'Int');
has 'name'             => (is => 'ro', isa => 'Str');
has 'shopname'         => (is => 'rw', isa => 'Str');
has 'pass'             => (is => 'ro', isa => 'Str');
has 'money'            => (is => 'rw', isa => 'Int');
has 'time'             => (is => 'rw', isa => 'Int');
has 'rank'             => (is => 'rw', isa => 'Int');
has 'rank_order'       => (is => 'rw', isa => 'Int');
has 'showcasecount'    => (is => 'rw', isa => 'Int');
has 'comment'          => (is => 'rw', isa => 'Str');
has 'saleyesterday'    => (is => 'rw', isa => 'Int');
has 'saletoday'        => (is => 'rw', isa => 'Int');
has 'costyesterday'    => (is => 'rw', isa => 'Int');
has 'rankingcount'     => (is => 'rw', isa => 'Int');
has 'remoteaddr'       => (is => 'rw', isa => 'Str');
has 'pointyesterday'   => (is => 'rw', isa => 'Int');
has 'rankingyesterday' => (is => 'rw', isa => 'Int');
has 'paytoday'         => (is => 'rw', isa => 'Int');
has 'payyesterday'     => (is => 'rw', isa => 'Int');
has 'profitstock'      => (is => 'rw', required => 0);
has 'boxcount'         => (is => 'rw');
has 'costtoday'        => (is => 'rw', isa => 'Int');
has 'options'          => (is => 'rw');
has 'taxyesterday'     => (is => 'rw', isa => 'Int');
has 'moneystock'       => (is => 'rw', isa => 'Int');
has 'taxtoday'         => (is => 'rw', isa => 'Int');
has 'guild'            => (is => 'rw');
has 'blocklogin'       => (is => 'rw');
has 'nocheckip'        => (is => 'rw');
has 'foundation'       => (is => 'rw', isa => 'Int');
has 'job'              => (is => 'rw', default => "none");
has 'icon'             => (is => 'rw', default => "none");
has 'showcase'         => (is => 'rw', isa => 'ArrayRef');
has 'rankupdown'       => (is => 'rw', isa => 'Str', default => "none");

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Shop::Entity - 店情報を保持するクラス

=head1 METHOD

=head1 AUTHOR

Fumihiro Ito

=cut
