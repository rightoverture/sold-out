package SoldOut::Environment;
use base qw/Exporter/;

our @EXPORT= qw/DEVELOP PRODUCT/;

sub DEVELOP {
    return 1 unless exists $ENV{PLACK_ENV};
    return ($ENV{PLACK_ENV} eq "development") ? 1 : 0;
}

sub PRODUCT {
    return (exists $ENV{PLACK_ENV} and $ENV{PLACK_ENV} eq "deployment") ? 1 : 0;
}

1;
