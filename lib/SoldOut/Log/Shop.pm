package SoldOut::Log::Shop;
use strict;
use warnings;
use Mouse;
use SoldOut::Config;

with 'SoldOut::Log::Base';

sub get {
    my $self = shift;
    my $shop_id = shift;

    return @{[]} unless $shop_id;

    my @result = $self->db->search(SoldOut::Config::LOG_TABLE,
        +{
            type => ['period', 'log'],
            mode => 0,
            send_from => ['0', $shop_id]
        }
    );

    return wantarray ? @result : \@result;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Log::Shop -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
