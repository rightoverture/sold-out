package SoldOut::Log::Base;
use strict;
use warnings;
use Mouse::Role;
use SoldOut::Config;
use SoldOut::DB;
use SoldOut::Log::Entity;

requires 'get';

has _dbh => (
    is      => 'ro',
);

has db => (
    is         => 'ro',
    lazy_build => 1,
);

around get => sub {
    my $called_func = shift;
    my $self = shift;

    my @result = $self->$called_func(@_);
    my @entities = map { $self->create_entity(%{$_->{row_data}}) } @result;

    return wantarray ? @entities : \@entities;
};

sub create_entity {
    my $self = shift;

    return SoldOut::Log::Entity->new(@_);
}

sub _build_db {
    my $self = shift;

    if ($self->_dbh) {
        return SoldOut::DB->new(dbh => $self->_dbh);
    } else {
        return SoldOut::DB->new(SoldOut::Config::CONNECT_INFO());
    }
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Log::Base -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
