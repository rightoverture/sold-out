package SoldOut::Log::Period;
use strict;
use warnings;
use SoldOut::Config;
use Mouse;

with 'SoldOut::Log::Base';

sub get {
    my $self = shift;

    my @result = $self->db->search(SoldOut::Config::LOG_TABLE,
        +{
            type => 'period',
            send_from => 'IS NOT NULL'
        }
    );

    return wantarray ? @result : \@result;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Log::Period -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
