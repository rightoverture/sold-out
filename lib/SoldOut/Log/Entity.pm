package SoldOut::Log::Entity;
use strict;
use warnings;
use Mouse;

has id => (
    is      => 'ro',
    isa     => 'Int',
);

has type => (
    is      => 'ro',
    isa     => 'Str',
);

has mode => (
    is      => 'ro',
    isa     => 'Int',
);

has send_from => (
    is      => 'ro',
    isa     => 'Int',
);

has send_to => (
    is      => 'ro',
    isa     => 'Int',
);

has message => (
    is      => 'ro',
    isa     => 'Str',
);

has insert_date => (
    is      => 'ro',
    isa     => 'Str',
);

no Mouse;
1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Log::Entity -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
