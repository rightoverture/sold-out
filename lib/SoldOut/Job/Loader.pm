package SoldOut::Job::Loader;
use strict;
use warnings;
use SoldOut::Config;

use parent qw/SoldOut::YAML::Loader/;

sub load {
    my $class = shift;

    return $class->SUPER::load(SoldOut::Config::JOB_CONFIG_PATH);
}

1;
