package SoldOut::Old::Util::Path;
use SoldOut::Old::_config;

sub GetPath {
    return $SoldOut::Old::_config::DATA_DIR."/".$_[0].$SoldOut::Old::_config::FILE_EXT if @_==1;
    return join("/",@_).$SoldOut::Old::_config::FILE_EXT;
}

1;
