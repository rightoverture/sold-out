package SoldOut::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterSimple;

# Controllers
use SoldOut::Web::Controller::Index;
use SoldOut::Web::Controller::Regist;
use SoldOut::Web::Controller::Office;
use SoldOut::Web::Controller::OfficeMoney;

sub get($;$$) { with_method('GET', @_); }
sub post($;$$) { with_method('POST', @_); }

get  '/' => "Index#top";
get  '/new' => "Regist#form";
post '/new/regist' => "Regist#sign_up";
post '/office/login' => "Office#login";
get  '/office' => "Office#main";
get  '/money-receive' => "OfficeMoney#receive";

# 開発環境の時のみ設定したルーティングをSTDERRに出力する
if ($ENV{PLACK_ENV} eq 'development') {
    warn router->as_string;
}

# dispatchメソッドの定義
{
    no strict 'refs';
    no warnings 'redefine';
    *{__PACKAGE__."::dispatch"} = \&_dispatch;
}

sub with_method {
    my ($method, $path, $dest, $opt) = @_;

    $opt->{method} = $method;
    if (ref $dest) {
        connect $path => $dest, $opt;
    } elsif (not $dest) {
        connect $path => {}, $dest;
    } else {
        my %dest;
        my ($controller, $action) = split('#', $dest);
        $dest{controller} = $controller;
        $dest{action} = $action if defined $action;
        connect $path => \%dest, $opt;
    }
}

sub _dispatch {
    my ($class, $c) = @_;
    my $req = $c->request;
    if (my $p = $class->match($req->env)) {
        my $action = $p->{action};
        $c->{args} = $p;
        "@{[ ref Amon2->context ]}::Controller::$p->{controller}"->$action($c, $p);
    } else {
        $c->res_404();
    }
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Dispatcher - Dispatcher

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head2 get

getのリクエストのルールを定義する

=head2 post

postのリクエストのルールを定義する

=head1 AUTHOR

Fumihiro Itoh

=cut
