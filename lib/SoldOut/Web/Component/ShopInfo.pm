package SoldOut::Web::Component::ShopInfo;
use strict;
use warnings;
use Mouse;

has shop => (
    is      => 'ro',
);

with 'SoldOut::Web::Component::Base';

sub render {
    my $self = shift;

    return (
        shop_info_rank => $self->shop->rank,
        shop_info_name => $self->shop->shopname,
        shop_info_money => $self->shop->money,
        shop_info_stock => $self->shop->moneystock,
        shop_info_time => $self->shop->action_time_str,
    );
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Component::ShopInfo -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
