package SoldOut::Web::Controller::Regist;
use strict;
use warnings;
use SoldOut::Game;
use SoldOut::Shop::Create;
use Data::Dumper;

sub form {
    my ($class, $c) = @_;

    return $c->render("regist_form.tmpl", +{
        title => SoldOut::Game::TITLE(),
    });
}

sub sign_up {
    my ($class, $c) = @_;

    if (not $c->req->param("password1")
            or not $c->req->param("password2")
                or ($c->req->param("password1") ne $c->req->param("password2"))) {
        # show form of sign up. because any errors occurred.
        return $c->render("regist_form.tmpl", +{
            title => SoldOut::Game::TITLE(),
            username => $c->req->param("username"),
            shop_name => $c->req->param("shop_name"),
        });
    }

    my $result = SoldOut::Shop::Create->new(dbh => $c->dbh)->setup(
        usernmae => $c->req->param("username"),
        shop_name => $c->req->param("shop_name"),
        password => $c->req->param("password1"),
        foundation => time(),
        remote_addr => $c->req->address,
    );

    return $c->render('regist_finish.tmpl', +{
        title => SoldOut::Game::TITLE(),
    });
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Controller::Regist -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
