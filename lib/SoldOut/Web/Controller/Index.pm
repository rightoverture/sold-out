package SoldOut::Web::Controller::Index;
use strict;
use warnings;
use utf8;
use List::MoreUtils qw//;
use Encode;
use Data::Dumper;

use SoldOut::Game;
use SoldOut::Shop::Image;
use SoldOut::Shop::Util;
use SoldOut::Item::Image;
use SoldOut::Log::Period;
use SoldOut::Shop::Builder::Director;
use SoldOut::Shop::Builder::Ranking;

sub top {
    my ($class, $c) = @_;

    my $people = $c->db->single('global', +{k => 'people'});

    my $director = SoldOut::Shop::Builder::Director->new(
        builder => SoldOut::Shop::Builder::Ranking->new
    );
    my $assign_ranking = $director->construct;

    my @log = SoldOut::Log::Period->new->get;

    return $c->render("index.tmpl", {
        title => SoldOut::Game::TITLE(),
        population => int($people->v / 10),
        ranking => $assign_ranking,
        log => \@log,
    });
}
1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Controller::Index - Controller of index page

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
