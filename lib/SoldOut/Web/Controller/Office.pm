package SoldOut::Web::Controller::Office;
use strict;
use warnings;
use SoldOut::Game;
use SoldOut::Auth::Authenticator;
use SoldOut::Log::Shop;
use SoldOut::Shop::Fetcher;

sub main {
    my ($class, $c) = @_;

    return $c->redirect("/") if not $c->session->get("shop_id");
    my $shop_id = $c->session->get("shop_id");

    my $shop = SoldOut::Shop::Fetcher->new(dbh => $c->dbh)->fetch($shop_id);

    my @log = SoldOut::Log::Shop->new->get($shop->id);

    return $c->render("office_main.tmpl", +{
        title => SoldOut::Game::TITLE,
        shop => $shop,
        log => \@log,
    });
}

sub login {
    my ($class, $c) = @_;

    return $c->redirect("/")
        if not defined $c->req->param("username") and not defined $c->req->param("password");

    my $shop_id = SoldOut::Auth::Authenticator->authenticate(
        $c->req->param("username"),
        $c->req->param("password"),
    );
    return $c->redirect("/") unless $shop_id;
    # set shop id to Plack::Session
    $c->session->set("shop_id", $shop_id);

    return $c->redirect("/office");
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Controller::Office -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
