package SoldOut::Web::Controller::OfficeMoney;
use strict;
use warnings;
use SoldOut::Game;
use SoldOut::Web::Component::ShopInfo;
use SoldOut::Shop::Builder::Director;
use SoldOut::Shop::Builder::Owner;

sub receive {
    my ($class, $c) = @_;

    return $c->redirect("/") if not $c->session->get("shop_id");
    my $shop_id = $c->session->get("shop_id");

    my $director = SoldOut::Shop::Builder::Director->new(
        builder => SoldOut::Shop::Builder::Owner->new,
    );
    my $shop = $director->construct($shop_id);

    return $c->render("office_money_receive.tmpl", +{
        title => SoldOut::Game::TITLE,
        SoldOut::Web::Component::ShopInfo->new(shop => $shop)->render,
    });
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Web::Controller::OfficeMoney -

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

Fumihiro Itoh

=cut
