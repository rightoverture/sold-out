package SoldOut::Auth::Authenticator;
use strict;
use warnings;
use SoldOut::Config;
use SoldOut::DB;
use Data::Dumper;

sub authenticate {
    my $class = shift;
    my $username = shift;
    my $password = shift;

    my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
    my $user = $db->single(SoldOut::Config::SHOP_TABLE, +{name => $username});

    return $user->pass eq $password ? $user->id : 0;
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut::Auth::Authenticator -

=head1 SYNOPSIS

    use SoldOut::Auth::Authenticator;

    my $is_auth = SoldOut::Auth::Authenticator->authenticate($username, $password);

=head1 DESCRIPTION

=head1 METHOD

=head2 authenticate

ユーザの認証を行う

ユーザが本物であればそのユーザのshop_idを返す．

=head1 AUTHOR

Fumihiro Itoh

=cut
