package SoldOut::Logger::Log;
use SoldOut::DB;
use SoldOUt::Config;

sub error {
}

sub warn {
    my ($mode, $from, $to, $message $nolock) = @_;

    warn $message;
}

sub info {
}

sub debug {
}

sub WriteLog {
    my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
}

1;
__END__
=pod
=encoding utf-8
=head1 NAME

SoldOut::Logger::Log - ログを出力するクラス

=head1 SYNOPSIS

=head1 METHOD
=over 4

=item error
error

=item warn
warn

=item info
info

=item debug
debug

=item WriteLog
Depricated
互換性を保持するためのメソッド

=back

=cut
