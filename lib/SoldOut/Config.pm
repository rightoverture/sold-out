package SoldOut::Config;
use SoldOut::Environment;

my $MODE;
BEGIN {
    $MODE = PRODUCT() ? 'product' : 'develop';
}

sub GAME_CONFIG {
    return 'global';
}

sub YAML_EXT {
    return '.yaml';
}

sub CONFIG_PATH {
    return '/Users/dexter/dev/fumihiro.ito/soldout/resources/';
}

sub ITEM_CONFIG_PATH {
    return 'item/';
}

sub JOB_CONFIG_PATH {
    return 'job'.SoldOut::Config::YAML_EXT;
}

sub CONNECT_INFO {
    return +{connect_info => ['dbi:mysql:database=soldout', 'root', '']};
}

sub SHOP_TABLE {
    return 'shop';
}

sub SHOWCASE_TABLE {
    return 'shop_showcase';
}

sub EXP_TABLE {
    return 'shop_exp';
}

sub SHOP_ITEM_TABLE {
    return 'shop_item';
}

sub LOG_TABLE {
    return 'log';
}

sub IMAGE_PATH {
    return 'image';
}

1;
__END__

=pod

=encoding utf-8

=head1 NAME

SoldOut::Config - 各種設定を保持するクラス

=head1 DESCRIPTION

ゲームバランスとは関係ない設定を保持する．
Deploy環境ではYAMLから設定を読み込み，Develop環境ではファイル内に記述された
設定を用いる．

=head1 AUTHOR

Fumihiro Ito

=cut
