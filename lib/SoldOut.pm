package SoldOut;
use strict;
use warnings;
use utf8;
use 5.010;
use SoldOut::DB;
use SoldOut::Config;

use parent qw(Amon2);

__PACKAGE__->load_plugin(qw/DBI/);

# initialize database
use DBI;
sub setup_schema {
    my $self = shift;
    my $dbh = $self->dbh();
    my $driver_name = $dbh->{Driver}->{Name};
    my $fname = lc("sql/${driver_name}.sql");
    open my $fh, '<:encoding(UTF-8)', $fname or die "$fname: $!";
    my $source = do { local $/; <$fh> };
    for my $stmt (split /;/, $source) {
        next unless $stmt =~ /\S/;
        $dbh->do($stmt) or die $dbh->errstr();
    }
}

sub db {
    my $self = shift;

    if( !defined $self->{db} ) {
        my $conf = $self->config->{'DBI'} or die "missing configuration";
        my $dbh = $self->dbh;
        $self->{db} = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
    }

    return $self->{db};
}

1;
__END__
=pod

=encoding utf-8

=head1 NAME

SoldOut - モダンSold Out

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHOD

=head1 AUTHOR

=cut
