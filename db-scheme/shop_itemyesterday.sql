DROP TABLE IF EXISTS shop_itemyesterday;
CREATE TABLE shop_itemyesterday (
    shop_id INT NOT NULL,
    item INT NOT NULL,
    num INT NOT NULL,
    PRIMARY KEY (shop_id, item)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
