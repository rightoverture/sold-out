DROP TABLE IF EXISTS market;
CREATE TABLE market (
    item_no INT NOT NULL,
    num INT DEFAULT 0,
    PRIMARY KEY (item_no)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
