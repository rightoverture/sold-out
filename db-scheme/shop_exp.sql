DROP TABLE IF EXISTS shop_exp;
CREATE TABLE shop_exp (
    shop_id INT NOT NULL,
    item INT NOT NULL,
    exp INT NOT NULL,
    PRIMARY KEY (shop_id, item)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
