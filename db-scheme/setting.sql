DROP TABLE IF EXISTS setting;
CREATE TABLE setting (
    k varchar(128) NOT NULL,
    v varchar(256) DEFAULT NULL,
    PRIMARY KEY (k)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
