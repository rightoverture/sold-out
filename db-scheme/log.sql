DROP TABLE IF EXISTS log;
CREATE TABLE log (
    id INT AUTO_INCREMENT,
    type varchar(16) NOT NULL,
    mode INT NOT NULL,
    send_from INT NOT NULL,
    send_to INT NOT NULL,
    message varchar(256) NOT NULL,
    insert_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
