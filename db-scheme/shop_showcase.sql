DROP TABLE IF EXISTS shop_showcase;
CREATE TABLE shop_showcase (
    id INT,
    shop_id INT NOT NULL,
    item INT NOT NULL,
    price INT NOT NULL,
    PRIMARY KEY (shop_id, id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
