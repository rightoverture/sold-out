DROP TABLE IF EXISTS item;
CREATE TABLE item (
    `no` INT(4) NOT NULL AUTO_INCREMENT,
    `sort` INT(3) NOT NULL,
    `type` INT(2) NOT NULL,
    `price` INT(8) NOT NULL,
    `cost` INT(8) NOT NULL,
    `pricebase` INT(8) NOT NULL,
    `pricehalf` INT(8) NOT NULL,
    `limit` INT(8) NOT NULL,
    `wslimit` INT(6) NOT NULL,
    `popular` INT(6) NOT NULL,
    `plus` INT(6) NOT NULL,
    `code` varchar(128) NOT NULL,
    `scale` varchar(128) NOT NULL,
    `name` varchar(128) NOT NULL,
    `info` varchar(256) NOT NULL,
    `point` INT(6) NOT NULL,
    `param` INT(4),
    `existimage` varchar(128),
    `func` INT(4),
    `functurn` INT(4),
    `funcsale` INT(4),
    `funcbuy` INT(4),
    `flag` TINYINT(4),
    PRIMARY KEY (no)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
