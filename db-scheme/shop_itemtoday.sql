DROP TABLE IF EXISTS shop_itemtoday;
CREATE TABLE shop_itemtoday (
    shop_id INT NOT NULL,
    item INT NOT NULL,
    num INT NOT NULL,
    PRIMARY KEY (shop_id, item)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
