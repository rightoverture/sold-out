DROP TABLE IF EXISTS shop_item;
CREATE TABLE shop_item (
    shop_id INT NOT NULL,
    item INT NOT NULL,
    num INT NOT NULL,
    exp INT NOT NULL,
    sales_today INT NOT NULL,
    sales_yesterday INT NOT NULL,
    PRIMARY KEY (shop_id, item)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
