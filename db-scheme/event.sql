DROP TABLE IF EXISTS event;
CREATE TABLE event (
    `id` INT(8) NOT NULL AUTO_INCREMENT,
    `code` varchar(32) NOT NULL,
    `group` INT(4) NOT NULL,
    `startproba` INT(8) NOT NULL,
    `startfunc` varchar(64),
    `startfuncparam` varchar(128),
    `endfunc` varchar(64),
    `endfuncparam` varchar(128),
    `basetime` INT(11),
    `plustime` INT(11),
    `startmsg` varchar(256),
    `endmsg` varchar(256),
    `func` varchar(64),
    `funcparam` varchar(128),
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

