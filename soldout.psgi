use Plack::App::CGIBin;
use Plack::Builder;
use File::Spec;
use File::Basename;

my $app = Plack::App::CGIBin->new(
    root => "/Users/dexter/dev/fumihiro.ito/soldout",
    exec_cb => sub { 1 },
)->to_app;
builder {
    enable 'Plack::Middleware::Static',
        path => qr{^(?:/image/)},
        root => File::Spec->catdir(dirname(__FILE__));
    mount "/" => $app;
};
