use strict;
use warnings;
use Plack::Builder;
use File::Spec;
use File::Basename;
use Plack::Session::Store::DBI;
use Plack::Session::State::Cookie;
use DBI;

use SoldOut;
use SoldOut::Web;

{
    my $c = SoldOut->new();
    $c->setup_schema;
}
my $db_config = SoldOut->config->{DBI} || die "Missing configuration for DBI";
builder {
    enable 'Plack::Middleware::Static',
        path => qr{^(?:/static/)},
        root => File::Spec->catdir(dirname(__FILE__));
    enable 'Plack::Middleware::Static',
        path => qr{^(?:/image/)},
        root => File::Spec->catdir(dirname(__FILE__));
    enable 'Plack::Middleware::Session',
        store => Plack::Session::Store::DBI->new(
            get_dbh => sub {
                DBI->connect( @$db_config )
                    or die $DBI::errstr;
            }
        ),
        state => Plack::Session::State::Cookie->new(
            httponly => 1,
        );
    SoldOut::Web->to_app();
};
