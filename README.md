ブラウザゲーム**SOLD OUT**をPlackへ移植する

# GOAL
[SOLD OUT](http://mutoys.com/)をPlackへ移植し，ゲームとして稼働することを目指す．

* ビジネスロジックの分離

トップディレクトリは最小のファイル構成にしたい．

* soldout.psgi以外コードがない状態
* cartonで依存モジュールの管理（環境の構築を容易にする）

# 実装方法など

* Mojolicious
* Teng
	* カスタマイズ性などを考慮しなるべく薄いWAFやラッパーを用いる
* Text::Haml
* Mouse

# cpanfileに記述出来ていない依存関係

動作時に必要なモジュールはcpanfileに記述されています．

* Carton v0.9.7
	* cpanfile形式で記述する為に必要


# インストール方法

OSXかつzshの場合．それ以外のOSは適宜読み替える

1. (install perlbrew)
2. perlbrew install perl-5.16.1
3. perlbrew use perl-5.16.1
4. perlbrew lib create soldout
5. perlbrew use perl-5.16.1@soldout
6. source ~/.zshrc
7. curl -L http://cpanmin.us | perl - --sudo App::cpanminus
8. sudo cpanm carton
9. carton install
10. carton exec -Ilib "plackup soldout.psgi"

## 開発時

1. cd /path/to/soldout
2. perlbrew use perl-5.16.1@soldout
3. carton exec -Ilib "plackup -r soldout.psgi"

## デプロイ時

あくまで予定

1. cd /path/to/soldout
2. perlbrew use perl-5.16.1@soldout
3. carton exec -Ilib "starman soldout.psgi"

## 依存モジュールを追加する時

1. vi cpanfile
2. carton install
3. git add cpanfile carton.lock
4. git commit -m "Add new MODULE"


# LOADMAP

1. Plack::App::CGIBinで実行出来ることを目指す
2. データをRDBMSへ移行
3. ビジネスロジックを分離
4. Amon2で画面を実装

# 元のデータファイル構造

# data/data.cgi

	1 $DTlasttime
	2 $DTpeople,$DTnextid,$DTblockip,$DTTradeIn,$DTTradeOut
	3 @DTwholestore
	4 %DTevent
	5 $DTtown
	6 $DT->{id},$DT->{lastlogin},$DT->{name},$DT->{shopname},$DT->{pass},$DT->{money},$DT->{time},$DT->{rank},$DT->{showcasecount},$DT->{comment},$DT->{saleyesterday},$DT->{saletoday},$DT->{costyesterday},$DT->{rankingcount},$DT->{remoteaddr},$DT->{pointyesterday},$DT->{rankingyesterday},$DT->{paytoday},$DT->{payyesterday},$DT->{profitstock},$DT->{boxcount},$DT->{costtoday},$DT->{options},$DT->{taxyesterday},$DT->{moneystock},$DT->{taxtoday},$DT->{guild},$DT->{blocklogin},$DT->{nocheckip},$DT->{foundation},$DT->{job},$DT->{icon}
	7 $DT->{showcase}:$DT->{price}:?:$DT->{itemyesterday}:$DT->{itemtoday}:$DT->{exp}:?

# data/log-s(1|2|3).cgi

	1 $NOW_TIME,$mode,$from,$to,$msg
	
# 設計

## フロント周り
* SoldOut
* SoldOut::Web
* SoldOut::Web::Dispatcher
* SoldOut::Web::Controller以下

## アイテム

* 設定ファイル
	* resources/item/hogehoge.yaml
	* resources/item_list.yaml

resources/item/hogehoge.yaml
	
	name: hogehoge
	type: 1
	price: 100
	cost: 100
	pricebase: 100
	pricehalf: 100
	limit: 10
	name: 草
	info: 野草
	…
	
resources/item_list.yaml

	item_list:
	  - hogehoge
	  - fugafuga
	  
* クラス
	* SoldOut::Item::ItemFactory
		* get_instance
	* SoldOut::Item::Item
	* SoldOut::Item::Loader
	* SoldOut::Item::Builder
	* SoldOut::Item::Builder::Role
	* SoldOut::Item::Director
	* SoldOut::Item::Unit - 単位を表す
	* SoldOut::Item::Unit::Loader
	
ItemとItemFactoryはSingletonオブジェクト．
FlyweightパターンでItemFactoryからItemオブジェクトを生成する．
ItemはItemFactory経由でインスタンスが生成される（Unit以下を除く）

## イベント

* 設定ファイル
	* resources/event/hogehoge.yaml
	* resources/event_list.yaml
	
resources/event/hogehoge.yaml

	code: hogehoge
	group: 1
	startproba: 1
	…
	
resources/event_list.yaml

	event_list:
	  - hogehoge
	  - fugafuga
	  
* クラス
	* SoldOut::Event::Event
	* SoldOut::Event::EventFactory
	* SoldOut::Event::Loader
	* SoldOut::Event::Builder

## JOB

* 設定ファイル
	* resources/job.yaml
	
resources/job.yaml

	hogehoge:
		name: ニート
		time_rate: 2

* クラス
	* SoldOut::Job
	* SoldOut::Job::Loader
	* SoldOut::Job::Entity

---

## 共通

* クラス
	* SoldOut::Config - ゲームバランスに関係ない設定を保持する
	* SoldOut::Config::Loader - 設定をYAMLから読み込む
	* SoldOut::Environment - 現在の環境を表す（Develop or Product）
	* SoldOut::YAML::Loader - YAMLを読み込む共通クラス

## Shop

* クラス
	* SoldOut::Shop::Entity
	* SoldOut::Shop::Stock::Entity
	* SoldOut::Shop::Showcase::Entity
	* SoldOut::Shop::DS
	* SoldOut::Shop::DS::Stock
	* SoldOut::Shop::DS::Showcase
	* SoldOut::Shop::DS::Shop
	* SoldOut::Shop::Ranking
	* SoldOut::Shop::Ranking::Yesterday
	* SoldOut::Shop::Ranking::Today
	* SoldOut::Shop::Register
	
## User

* クラス
	* SoldOut::User
	* SoldOut::User::Entity
	* SoldOut::User::Register
	
## Market

* クラス
	* SoldOut::Market
	* SoldOut::Market::Entity
	* SoldOut::Market::Analyze

---
	
# 作業状況
## DBからデータを引いてくるように変更済み
* index.cgi
* main.cgi
* moneystock.cgi
* log.cgi
* showcase.cgi
* ranking.cgi
* shop.cgi
* analyze.cgi

## 未着手
* admin-sub.cgi
* admin-sub2.cgi
* admin.cgi
* bbs.cgi
* box-edit.cgi
* box.cgi
* buy-s.cgi
* buy.cgi
* chat.cgi
* commentlist.cgi
* counter.cgi
* custom.cgi
* gmsg-s.cgi
* gmsg.cgi
* guild.cgi
* help.cgi
* item-send.cgi
* item-use.cgi
* item.cgi
* jump.cgi
* makeitem.cgi
* market.cgi
* menu.cgi
* move-town.cgi
* new.cgi
* other.cgi
* port.cgi
* recv-shop.cgi
* shop-master.cgi
* showcase-edit.cgi
* trade-s.cgi
* trade.cgi
* user.cgi
