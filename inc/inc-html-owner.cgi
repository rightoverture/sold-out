my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);

my $shop = $db->single('shop', +{name => $USER});
$disp.="●店舗情報<HR>";
$disp.='未入店自動閉店期限:'.GetTime2FormatTime($NOW_TIME+$EXPIRE_TIME+GetExpireTimeExtend($shop)).'<br>';
my $tm=$NOW_TIME-$shop->time;
if ($tm<0) {
    $tm=-$tm;
    $tm='行動可能まであと '.GetTime2HMS($tm);
} else {
    if($tm>$MAX_STOCK_TIME){$tm=$MAX_STOCK_TIME;}
    $tm=GetTime2HMS($tm);
}
my $rankmsg=GetRankMessage($shop->rank);
my ($tax, $taxrate)=GetTaxToday($shop);
if($taxrate) {
	$taxrate = "税率:$taxrate\%";
}else{
	$taxrate="免税";
}

my $expsum=0;
my @shop_exp = $db->search('shop_exp', +{shop_id => $shop->id});
foreach (@shop_exp) {
    $expsum += $_->exp;
}
$expsum=int($expsum/10)."%";
my $job=$JOBTYPE{$shop->job};
$job ||= '不定';

if(!$MOBILE) {
	$disp.=$TB;
	$disp.=$TR.$TD."名前".$TD.$shop->name.$TD."店名".$TD.GetTagImgGuild($shop->guild).$shop->shopname.$TRE;
	$disp.=$TR.$TD."RANK".$TD.$shop->rank_order.$TD."TOP".$TD.$shop->rankingcount."回 ".GetTopCountImage($shop->rankingcount).$TRE;
	$disp.=$TR.$TD."人気".$TD.$rankmsg.$TD."資金/入金庫".$TD."\\".$shop->money."/\\".$shop->moneystock.$TRE;
	$disp.=$TR.$TD."今期売上".$TD."\\".$shop->saletoday.$TD."前期売上".$TD."\\".$shop->saleyesterday.$TRE;
	$disp.=$TR.$TD."今期支払".$TD."\\".$shop->paytoday.$TD."前期支払".$TD."\\".$shop->payyesterday.$TRE;
	$disp.=$TR.$TD."持ち時間".$TD.$tm.$TD."点数".$TD.$shop->point.$TRE;
	$disp.=$TR.$TD."今期維持費<BR><SMALL>(決算時徴収)</SMALL>".$TD."\\".int($shop->costtoday)."+\\".$SHOWCASE_COST[$shop->showcasecount-1];
	$disp.=    $TD."前期維持費".$TD."\\".$shop->costyesterday.$TRE;
	$disp.=$TR.$TD."今期税金<BR><SMALL>(決算時徴収)</SMALL>".$TD."\\".$tax."<br><small>$taxrate</small>".$TD."前期税金".$TD."\\".($shop->taxyesterday+0).$TRE;
	$disp.=$TR.$TD."支払済売却税".$TD."\\".$shop->taxtoday.$TD."基本売却税率".$TD.GetUserTaxRate($shop).'%'.$TRE;
	$disp.=$TR.$TD."熟練度合計".$TD.$expsum;
	$disp.=    $GUILD{$shop->guild} ? $TD."ギルド会費 <SMALL>売上の".($GUILD{$shop->guild}->[$GUILDIDX_feerate]/10)."%<br>(決算時徴収)</SMALL>".$TD.'\\'.int($shop->saletoday*$GUILD{$shop->guild}->[$GUILDIDX_feerate]/1000) : $TD."　".$TD."　";
	$disp.=  $TRE;
	$disp.=$TR.$TD.'創業'.$TD.GetTime2HMS($NOW_TIME-$shop->foundation).$TD.'職業'.$TD.$job.$TRE;
	$disp.=$TBE;
}
else
{
	$disp.="名前:".$shop->name."<BR>";
	$disp.="店名:".GetTagImgGuild($shop->guild).$shop->shopname."<BR>";
	$disp.="RANK:".$shop->rank_order."<BR>";
	$disp.="TOP :".($shop->rankingcount+0)."回<BR>";
	$disp.="人気:".$rankmsg."<BR>";
	$disp.="資金:"."\\".$shop->money."<BR>";
	$disp.="入金:"."\\".$shop->moneystock."<BR>";
	$disp.="今売:"."\\".$shop->saletoday."<BR>";
	$disp.="会費:"."\\".int($shop->saletoday*$GUILD{$shop->guild}->[$GUILDIDX_feerate]/1000)."<BR>" if $shop->guild ne '';
	$disp.="前売:"."\\".$shop->saleyesterday."<BR>";
	$disp.="今払:"."\\".$shop->paytoday."<BR>";
	$disp.="前払:"."\\".$shop->payyesterday."<BR>";
	$disp.="今維:"."\\".int($shop->costtoday)."+\\".$SHOWCASE_COST[$shop->showcasecount-1]."<BR>";
	$disp.="前維:"."\\".$shop->costyesterday."<BR>";
	$disp.="済税:"."\\".($shop->taxtoday+0)."<BR>";
	$disp.="今税:"."\\".$tax."(".$taxrate.")<BR>";
	$disp.="前税:"."\\".($shop->taxyesterday+0)."<BR>";
	$disp.="時間:".$tm."<BR>";
	$disp.="点数:".$shop->point."<BR>";
	$disp.="熟練:".$expsum."<BR>";
	$disp.="創業:".GetTime2HMS($NOW_TIME-$shop->foundation)."<BR>";
	$disp.="職業:".$job."<BR>";
}

1;
