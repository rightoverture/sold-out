my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
my @shops = $db->search('shop', +{}, +{
        order_by => [{'rankingyesterday' => 'ASC'}, {'point' => 'DESC'}]
    }
);
my $usercount = $db->count('shop', '*');

RequireFile('inc-html-ownerinfo.cgi');

my($page,$pagestart,$pageend,$pagenext,$pageprev,$pagemax);

if($Q{ds}!=0)
{
	my($id,$idx)=CheckUserID($Q{ds});
	($page,$pagestart,$pageend,$pagenext,$pageprev,$pagemax)=
		(0,$idx,$idx,0,0,0)
}
else
{
	($page,$pagestart,$pageend,$pagenext,$pageprev,$pagemax)=
		GetPage($Q{pg},$SHOP_PAGE_ROWS,$usercount);
}

$disp.="●他店";
$disp.="<HR SIZE=\"1\">";

my $pagecontrol=GetPageControl($pageprev,$pagenext,"","",$pagemax,$page);
$disp.=$pagecontrol."<HR SIZE=\"1\">" if $pagecontrol ne '';

#foreach my $cnt ($pagestart .. $pageend) {
foreach my $shop (@shops) {
	$disp.="RANK ".($shop->rank_order)." ";
	$disp.="<BR>" if $MOBILE;
	$disp.=GetTagImgGuild($shop->guild)." ".$shop->shopname."<BR>";
	$disp.="一言:$shop->comment<BR>" if $shop->comment;

	$disp.=$TB;
    my @shop_showcase = $db->search('shop_showcase', +{shop_id => $shop->id}, +{order_by => 'item'});
    my @shop_item = $db->search('shop_item', +{shop_id => $shop->id});
    my %shop_have;
    foreach my $item (@shop_item) {
        $shop_have{$item->item} = {} if not exists $shop_have{$item->item};
        $shop_have{$item->item}->{num} = $item->num;
    }
    my @item_today = $db->search('shop_itemtoday', +{shop_id => $shop->id});
    foreach my $item (@item_today) {
        $shop_have{$item->item} = {} if not exists $shop_have{$item->item};
        $shop_have{$item->item}->{today} = $item->num;
    }
    my $idx = 0;
    foreach my $showcase_item (@shop_showcase) {
		my $itemno = $showcase_item->item;
		if ($itemno) {
			my $ITEM=$ITEM[$itemno];
			my $nobuy=CheckItemFlag($itemno,'nobuy');
			$stock=$show_have{$itemno}->{num};
			$disp.=$TR.$TD;
			$disp.="<A HREF=\"buy.cgi?buy=$shop->id!$idx!$itemno&bk=p!$page&$USERPASSURL\">" if $stock && !$GUEST_USER && !$nobuy;
			$disp.=GetTagImgItemType($itemno).$ITEM->{name};
			$disp.='(購入不可)' if $nobuy;
			$disp.="</A>" if $stock && !$GUEST_USER && !$nobuy;
			$disp.=$TD."\@\\".$showcase_item->price;
			my $msg=$stock ? "残".$stock.$ITEM->{scale} : "SOLD OUT";
			$disp.=$TD.$msg;
			$disp.=$TD.($shop_have{$itemno}->{today}+0).$ITEM->{scale}."売上";
			$disp.=$TRE;
		}
        $idx++;
	}
	$disp.=$TBE;
	$disp.="<HR SIZE=1>";
}

$disp.=$pagecontrol;

1;
