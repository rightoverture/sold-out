my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
my $shop = $db->single('shop', +{name => $USER});

RequireFile('inc-html-ownerinfo.cgi');

my $tp=$Q{tp};
my $pg=$Q{pg};

my @shop_item = $db->search('shop_item', +{shop_id => $shop->id});
my @shop_item_today = $db->search('shop_itemtoday', +{shop_id => $shop->id});
my @shop_item_yesterday = $db->search('shop_itemyesterday', +{shop_id => $shop->id});

my $DTitem=$DT->{item};
my $DTitemtoday=$DT->{itemtoday};
my $DTitemyesterday=$DT->{itemyesterday};
my $DTexp=$DT->{exp};

GetMarketStatus();

my @shop_showcase = $db->search('shop_showcase', +{shop_id => $shop->id});
my %showcase=();
my $itemno;
foreach(0..$shop->showcasecount-1)
{
	next if !($itemno=$shop_showcase[$_]->item);
	$showcase{$itemno}.="棚".($_+1)."\\".$shop_showcase[$_]->price." ";
}
$disp.="●倉庫<HR>";
foreach my $cnt (0..$#ITEMTYPE)
{
	my $name=$ITEMTYPE[$cnt];
	$name="&lt;".$name."&gt;" if $cnt==$tp;
	$name=GetTagImgItemType(0,$cnt).$name if $cnt && !$MOBILE;
	$name="<A HREF=\"$MYNAME?$USERPASSURL&tp=$cnt\">".$name."</A>" if $cnt!=$tp;
	
	$disp.=$name." ";
}
$disp.="<BR>";

my @itemlist=(1..$MAX_ITEM);
@itemlist=grep($ITEM[$_]->{type}==$tp,@itemlist) if $tp;
@itemlist=grep(
	(
		$DTitem->[$_-1] ||
		$DTitemtoday->{$_} ||
		$DTitemyesterday->{$_} ||
		$DTexp->{$_}
	),
	@itemlist
);

my %item_list;
my @have_item_list = $db->search('shop_item', +{shop_id => $shop->id});
foreach $item (@have_item_list) {
    $item_list{$item->item} = {} if not exists $item_list[$item->item];
    $item_list{$item->item}->{num} = $item->num;
}
my @have_exp_item_list = $db->search('shop_exp', +{shop_id => $shop->id});
foreach $item (@have_exp_item_list) {
    $item_list{$item->item} = {} if not exists $item_list[$item->item];
    $item_list{$item->item}->{exp} = $item->exp;
}
my @item_yesterday = $db->search('shop_itemyesterday', +{shop_id => $shop->id});
foreach $item (@have_exp_item_list) {
    $item_list{$item->item} = {} if not exists $item_list[$item->item];
    $item_list{$item->item}->{yesterday} = $item->num;
}
my @item_today = $db->search('shop_itemtoday', +{shop_id => $shop->id});
foreach $item (@have_exp_item_list) {
    $item_list{$item->item} = {} if not exists $item_list[$item->item];
    $item_list{$item->item}->{today} = $item->num;
}

if(!@itemlist)
{
	$disp.="<HR>在庫がありません";
}
else
{
	#@itemlist=sort{ $ITEM[$a]->{sort} <=> $ITEM[$b]->{sort} } @itemlist; # 特定環境でエラーになるため下記で代替
	my @sort;
	foreach(@itemlist){$sort[$_]=$ITEM[$_]->{sort}};
	@itemlist=sort{ $sort[$a] <=> $sort[$b] } @itemlist;
	
	my($page,$pagestart,$pageend,$pagenext,$pageprev,$pagemax)
		=GetPage($pg,$LIST_PAGE_ROWS,scalar(@itemlist));
	
	$disp.=$TB;
	if($MOBILE)
	{
		$tdh_sp="標準:";
		$tdh_cs="維持:";
		$tdh_st="在庫:";
		$tdh_ts="本昨売:";
		$tdh_ex="熟練:";
		$tdh_sc="陳列:";
		$tdh_mp="相場:";
		$tdh_mb="需給:";
	}
	else
	{
		$disp.=$TR.$TD.
			join($TD,
				qw(
					名称
					標準<br>価格
					維持費<br>1個24時間分
					在庫数<br>/最大
					今期<br>前期<br>売上
					熟練度
					陳列
					販売価格<br>相場
					需要供給<br>バランス
				)
			).$TRE;
	}
	
    #foreach my $cnt (map{$itemlist[$_]}($pagestart..$pageend))
    #{
        #my $ITEM=$ITEM[$cnt];
        
        #my $name=GetTagImgItemType($cnt).$ITEM->{name};
        #$name="<A HREF=\"item.cgi?no=$cnt&bk=s!$page!$tp&$USERPASSURL\">".$name."</A>" if $DTitem->[$cnt-1];
        
        #$disp.=$TR.$TD.
            #join($TD,
                #$name,
                #$tdh_sp."\\".$ITEM->{price},
                #$tdh_cs."\\".$ITEM->{cost},
                #$tdh_st.$DTitem->[$cnt-1]."/".$ITEM->{limit},
                #$tdh_ts.($DTitemtoday->{$cnt} || $DTitemyesterday->{$cnt} ? ($DTitemtoday->{$cnt}||0)."/".($DTitemyesterday->{$cnt}||0) : '　'),
                #$tdh_ex.($DTexp->{$cnt} ? int($DTexp->{$cnt}/10)."%" : '　'),
                #$tdh_sc.($showcase{$cnt}||'　'),
                #$tdh_mp.($ITEM->{marketprice} ? "\\".$ITEM->{marketprice} : '　'),
            #).
            #$TDNW.$tdh_mb.GetMarketStatusGraph($ITEM->{uppoint}||=10).
            #$TRE;
    #}
    foreach my $key (sort keys %item_list) {
        my $ITEM = $ITEM[$key];
        my $name = GetTagImgItemType($key).$ITEM->{name};
        $name = "<A HREF=\"item.cgi?no=$key&bk=s!$page!$tp&$USERPASSURL\">".$name."</A>" if $item_list{$key}->{num} > 0;
        
        $disp.=$TR.$TD.
            join($TD,
                $name,
                $tdh_sp."\\".$ITEM->{price},
                $tdh_cs."\\".$ITEM->{cost},
                $tdh_st.$item_list{$key}->{num}."/".$ITEM->{limit},
                $tdh_ts.($item_list{$key}->{today} || $item_list{$key}->{yesterday} ? ($item_list[$key]->{today}||0)."/".($item_list{$key}->{yesterday}||0) : '　'),
                $tdh_ex.($item_list{$key}->{exp} ? int($item_list{$key}->{exp}/10)."%" : '　'),
                $tdh_sc.($showcase{$key}||'　'),
                $tdh_mp.($ITEM->{marketprice} ? "\\".$ITEM->{marketprice} : '　'),
            ).
            $TDNW.$tdh_mb.GetMarketStatusGraph($ITEM->{uppoint}||=10).
            $TRE;
    }
	$disp.=$TBE;
	
	$disp.=GetPageControl($pageprev,$pagenext,"tp=$tp","",$pagemax,$page);
}

1;
