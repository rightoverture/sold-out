my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
my $shop = $db->single('shop', +{name => $USER});

if(!$GUEST_USER) {
	my $tm=$NOW_TIME-$shop->time;
	if($tm<0)
	{
		$tm=-$tm;
		$tm='行動可能まであと '.GetTime2HMS($tm);
	}
	else
	{
		$tm=$MAX_STOCK_TIME if $tm>$MAX_STOCK_TIME;
		$tm=GetTime2HMS($tm);
	}
	my $rankmsg=GetRankMessage($shop->rank);
	
	$disp.=<<STR;
	●店舗情報<BR>
	$TB$TR
	$TD
	RANK ${\($shop->rank_order)}$TDE
	$TD店名:${\($shop->shopname)}$TDE
	$TD資金:\\${\($shop->money)}$TDE
	$TD入金庫:\\${\($shop->moneystock)}$TDE
	$TD時間:$tm$TDE
	$TRE$TBE
	<HR SIZE=1>
STR
}
1;
