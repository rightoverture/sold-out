my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);

my $shop = $db->single('shop', +{name => $USER});

my $showcasecount=$shop->showcasecount;

$disp.="●陳列棚：現在$showcasecount個：維持費 \\$SHOWCASE_COST[$showcasecount-1]<HR>";

my $usertaxrate=GetUserTaxRate($shop);

$disp.=$TB;
if(!$MOBILE)
{
	$disp.=$TR;
	$disp.=$TD."棚No";
	$disp.=$TD."商品名";
	$disp.=$TD."売値";
	$disp.=$TD."標準価格";
	$disp.=$TD."売却税";
	$disp.=$TD."在庫数";
	$disp.=$TD."今期売上数";
	$disp.=$TD."前期売上数";
	$disp.=$TD;
	$disp.=$TRE;
}
else
{
	$tdh_pr{$MOBILE}="売値:";
	$tdh_sp{$MOBILE}="標準:";
	$tdh_tx{$MOBILE}="売税:";
	$tdh_st{$MOBILE}="在庫:";
	$tdh_ts{$MOBILE}="本売:";
	$tdh_ys{$MOBILE}="昨売:";
}

my @show_cases = $db->search('shop_showcase', +{shop_id => $shop->id});
my @item_yesterday = $db->search('shop_itemyesterday', +{shop_id => $shop->id});
my @item_today = $db->search('shop_itemtoday', +{shop_id => $shop->id});
for (my $cnt = 0; $cnt < scalar @show_cases; $cnt++) {
    my $show_case = $show_cases[$cnt];
	my $itemno = $show_case->item;
	my $ITEM = $ITEM[$itemno];
	my $scale = $ITEM->{scale};
    my $stock_data = $db->single('shop_item', +{shop_id => $shop->id, item => $show_case->item});
	my $stock = $itemno ? $stock_data->num : 0;

	$disp.=$TR.$TD."棚".($cnt+1);
	$disp.=$TD;
	$disp.="<A HREF='item.cgi?$USERPASSURL&no=$itemno&sc=".($cnt+1)."&pr=".$show_case->price."&bk=$MYNAME'>" if $stock;
	$disp.=GetTagImgItemType($itemno).$ITEM->{name};
	$disp.="</A>" if $stock;
	
	if ($itemno) {
		my($taxrate,$tax)=GetSaleTax($itemno, 1, $show_case->price, $usertaxrate);
		$disp.=$TD.$tdh_pr{$MOBILE}."\\".$show_case->price;
		$disp.=$TD.$tdh_sp{$MOBILE}."\\".$ITEM->{price};
		$disp.=$TD.$tdh_tx{$MOBILE}."\\".$tax." (税率".$taxrate."%)";
		$disp.=$TD.$tdh_st{$MOBILE}.$stock.$scale;
        my @tmp = grep {$_->item == $itemno} @item_today;
		$disp.=$TD.$tdh_ts{$MOBILE}.(defined $tmp[0] ? $tmp[0]->num : 0).$scale;
        @tmp = grep {$_->item == $itemno} @item_yesterday;
		$disp.=$TD.$tdh_ys{$MOBILE}.(defined $tmp[0] ? $tmp[0]->num : 0).$scale;
		$disp.=$TD."<A HREF='showcase-edit.cgi?yen=1&$USERPASSURL&item=0&no=$cnt&bk=sc'>陳列中止</A>";
	}
	else
	{
		$disp.=$TD.$TD.$TD.$TD.$TD.$TD;
	}
	$disp.=$TRE;
}

$disp.=$TBE;

1;
