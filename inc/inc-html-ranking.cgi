my $db = SoldOut::DB->new(SoldOut::Config::CONNECT_INFO);
my ($page,  $pagestart,  $pageend, $pagenext, $pageprev, $pagemax) = GetPage(
    $Q{pg},
    ($MYNAME eq 'index.cgi' ? $TOP_RANKING_PAGE_ROWS : $RANKING_PAGE_ROWS),
    $db->count('shop', '*'),
);

my $people = $db->single('global', +{k => 'people'});
$disp .= "●ランキング<HR>";
$disp .= "人口:".int($people->v/10)."<BR>";

my $pagecontrol = GetPageControl($pageprev, $pagenext, "", "", $pagemax, $page);
$disp .= $pagecontrol."<BR>";

$disp .= $TB;

if(!$MOBILE)
{
	$disp .= $TR;
	$disp .= $TDNW."順位<BR><SMALL>(前期比)</SMALL><br>点数";
	$disp .= $TDNW.$SHOP_ICON_HEADER if $SHOP_ICON_HEADER && !$MOBILE;
	$disp .= $TDNW."店名<BR>人気";
	$disp .= $TDNW."今期売上";
	$disp .= $TDNW."資金<BR>前期売上";
	$disp .= $TDNW."前期<BR>維持費<BR>税金";
	$disp .= $TD."取扱商品　一押商品<BR>【熟練度合計】【創業】 コメント";
	$disp .= $TRE;
}
else
{
	$tdh_rk = "RANK:";
	$tdh_pt = "点数:";
	$tdh_nm = "店名:";
	$tdh_pp = "人気:";
	$tdh_mo = "資金:";
	$tdh_ts = "本売:";
	$tdh_ys = "昨売:";
	$tdh_cs = "維持:";
	$tdh_sc = "一押:";
	$tdh_cm = "一言:";
	$tdh_tx = "昨税:";
	$tdh_ex = "熟練:";
	$tdh_fd = "創業:";
}

my @shops = $db->search('shop', +{}, +{
        order_by => [{'rankingyesterday' => 'ASC'}, {'point' => 'DESC'}]
    }
);

foreach my $idx ($pagestart..$pageend) {
    my $DT=$DT[$idx];
    my $shop = $shops[$idx];

    my $rankupdown = "";
    if ($shop->rankingyesterday) {
        $rankupdown = $shop->rankingyesterday - $idx - 1;
        $rankupdown = $rankupdown == 0 ? "→": $rankupdown<0 ? "↓".(-$rankupdown) : "↑".$rankupdown;
        $rankupdown ="<small>($rankupdown)</small>";
    }
    my $itemtype = -1;
    my $itempro = "";
    my $salelist = "";

    my @showcase = $db->search('shop_showcase', +{shop_id => $shop->id});
    foreach my $no (@showcase) {
        $salelist .= GetTagImgItemType($no->item);
        if ($itemtype != -1 && $ITEM[$no->item]->{type} != $itemtype) {
            $itemtype = 0;
            next;
        }
        $itemtype = $ITEM[$no->item]->{type};
    }
    $itempro = GetTagImgItemType(0, $itemtype, 1)." " if $itemtype;
    my $itemno = $showcase[0]->item;
    $salelist .= " ".$ITEM[$itemno]->{name}." \\".$showcase[0]->price if $itemno;

    my $expsum=0;
    my @shop_exp = $db->search('shop_exp', +{shop_id => $shop->id});
    foreach (@shop_exp) {
        $expsum += $_->exp;
    }
    $expsum = "【".int($expsum/10)."%】";

    my $job = $JOBTYPE{$shop->job};
    $job = "【$job】" if $job;

    $disp .= $TR;
    $disp .= $TDNW.$tdh_rk."<b>".($idx+1)."</b>".$rankupdown."<br>";
    $disp .=     $tdh_pt.$shop->point;
    $disp .= $TD.GetTagImgShopIcon($shop->icon) if $SHOP_ICON_HEADER && !$MOBILE;
    $disp .= $TD.$tdh_nm;
    $disp .=     "<a href=\"shop.cgi?ds=$shop->id&$USERPASSURL\">" if !$GUEST_USER;
    $disp .=     GetTagImgGuild($shop->guild).$itempro.$job.$shop->shopname."[".$shop->name."]";
    $disp .=     "</a>" if !$GUEST_USER;
    $disp .= GetTopCountImage($shop->rankingcount+0) if $shop->rankingcount;
    $disp .= "<BR>";
    $disp .=     $tdh_pp.GetRankMessage($shop->rank);
    $disp .= $TDNW.$tdh_ts."\\".$shop->saletoday;
    $disp .= $TDNW.$tdh_mo.GetMoneyMessage($shop->money)."<BR>";
    $disp .=     $tdh_ys."\\".$shop->saleyesterday;
    $disp .= $TDNW.$tdh_cs."\\".($shop->costyesterday+0)."<br>";
    $disp .=     $tdh_tx."\\".($shop->taxyesterday+0);

    $disp .= $TD;

    $disp .= $tdh_sc.$salelist;

    $disp .= "<BR>";

    $disp .= $tdh_ex.$expsum;
    $disp .= $tdh_fd."【".GetTime2HMS($NOW_TIME-$shop->foundation)."】";
    $disp .= $tdh_cm.$shop->comment if $shop->comment;
    $disp .= $TRE;
}
$disp .= $TBE;

$disp .= $pagecontrol;

1;
